import string


def lisAlphabet():
    return list(string.ascii_uppercase)

def chiffrement_vigenere(cle, texte):
    alphabet = lisAlphabet()
    texte_chiffre = ""

    for i in range(len(texte)):
        if texte[i] in alphabet:
            lettre_texte = texte[i]
            lettre_cle = cle[i % len(cle)]
            decalage = alphabet.index(lettre_cle)
            lettre_chiffre = alphabet[(alphabet.index(lettre_texte) + decalage) % 26]
            texte_chiffre += lettre_chiffre
        else:
            texte_chiffre += texte[i]

    return texte_chiffre

def Dechiffrement_vigenere(cle, texte):
    alphabet = lisAlphabet()
    texte_chiffre = ""

    for i in range(len(texte)):
        if texte[i] in alphabet:
            lettre_texte = texte[i]
            lettre_cle = cle[i % len(cle)]
            decalage = alphabet.index(lettre_cle)
            lettre_chiffre = alphabet[(alphabet.index(lettre_texte) - decalage) % 26]
            texte_chiffre += lettre_chiffre
        else:
            texte_chiffre += texte[i]

    return texte_chiffre

texte_chiffre = chiffrement_vigenere('CRYPTO', 'MATHEMATIQUES')
print("Texte chiffré:", texte_chiffre)


#On connaît la longueur de la clé (|k| = 4 ) et le message chiré (c=XOZHDIMOYEL )
#(a) Combien y a-t-il de clés k possibles? 26^4
#(b) On sait que le message en clair m est soit EXPONENTIEL soit LOGARITHMES. Quel est le message envoyé?

def dechiffrement_vigenere(texte, cle):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    texte_dechiffre = ''

    for i in range(len(texte)):
        if texte[i] in alphabet:
            lettre_texte = texte[i]
            lettre_cle = cle[i % len(cle)]
            decalage = alphabet.index(lettre_cle)
            lettre_dechiffre = alphabet[(alphabet.index(lettre_texte) - decalage) % 26]
            texte_dechiffre += lettre_dechiffre
        else:
            texte_dechiffre += texte[i]

    return texte_dechiffre

message_chiffre = "XOZHDIMOYEL"
cle_possible = ""

# On va tester toutes les combinaisons possibles avec les deux mots candidats
for mot_candidat in ["EXPONENTIEL", "LOGARITHMES", "CACA"]:
    for i in range(len(message_chiffre)):
        lettre_message = message_chiffre[i]
        lettre_cle = mot_candidat[i % len(mot_candidat)]
        decalage = ord(lettre_message) - ord(lettre_cle)
        cle_possible += chr(((decalage % 26) + 26) % 26 + ord('A'))

    texte_dechiffre = dechiffrement_vigenere(message_chiffre, cle_possible)

    print(f"Avec la clé {cle_possible}, le texte déchiffré est : {texte_dechiffre}")

    # Remettre la clé_possible à une chaîne vide pour le prochain candidat
    cle_possible = ""


print(Dechiffrement_vigenere('SECRET','EEVYIFSXKHYXK'))


