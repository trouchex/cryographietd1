# Il y a 26!

import string
def lisAlphabet():
    return list(string.ascii_uppercase)

alphabetModifier =  'CWHORLFMUZXAEIYJKDVNPQSTGB'
def ChiffrementPermutation(k, m):
    chaineCrypte = ""
    for l in m:
        for letre in lisAlphabet():
            if l==letre:
                chaineCrypte += k[lisAlphabet().index(l)]
    return chaineCrypte

print(ChiffrementPermutation(alphabetModifier, 'PERMUTATION'))
print(ChiffrementPermutation(alphabetModifier,'ARRANGEMENT'))

def DechiffrementPermutation(k,m):
    chaineDecrypte = ""

    for l in m:
        for letre in lisAlphabet():
            if l == letre:
                chaineDecrypte += lisAlphabet()[k.index(l)]
    return chaineDecrypte

print(DechiffrementPermutation(alphabetModifier, 'ORIYEWDRERIN'))
print(DechiffrementPermutation(alphabetModifier, 'JRDEPNCNUYI'))
print(DechiffrementPermutation(alphabetModifier, 'CDDCIFRERIN'))

messageChiffre = 'UXPFFPVTXFKGFSULKGHHPNXFTUGNLXUPDBUXSLPLUFLHPUXZLPNBFVDPYBXVVPLUDPYUGVBXVHPNPDBMXPGUNXFBXUPNLTUBMBXDNPELUPBLNPVUPYBVNPDWISXPFPNPVKGFMPUVBTXGFVNPEXVTUGTZLBFNDPVHBTWPHBTXZLPVVGFTXVVLPVNLFPBEVTUBKTXGFPVVPFTXPDDPTGLTPKWGVPVPUBHPFPBVPVYUGYUXPTPVSPGHPTUXZLPVGLBDSPEUXZLPVVBKWBFTZLPDPVLFPVDBVTUGFGHXPDBFBMXSBTXGFDBUKWXTPKTLUPDPVRBXTVVGKXBLOKGHHPDBNPHGSUBYWXPDPKGHHPUKPBLDGXFDXHYGTBYYPDDPFTDPKBDKLDVIYUPTPFTTBFNXVZLPDPOXSLXTPDXFVXSFXRXBFKPDBHGFGTGFXPNPDBMXPXFNXMXNLPDDPVBKKGHHGNPFTVYGFTBFPHPFTNLFPBYYUPWPFVXGFXFTLXTXMPBYYUGOXHBTXMPTGLTPYUBTXZLP'

# a poursuivre
#for mot_possible in alphabetModifier:
 #   for i in range(len(messageChiffre)):

